

# Microservicios con Flask 

#Operaciones Matematicas implementadas con arquitectura en microservicios
Este es un proyecto en el que se implementa una arquitectura basada en microservicios en una maquina linux Ubuntu 18.04.4 donde las operaciones matematicas basicas se distribuyen en diferentes apis y estas son consumidas en una aplicación web a traves de un gateway.


## Tecnologias

### APIS
Se desarrollaron cuatro apis, cada una cumple la funcion de una operacion matematica basica (sumar, restar, multiplicar, dividir), la tecnologia utilizada para la creación de estas apis es FLASK con python 2.7 creadas cada una en contenedores de docker.

### API GATEWAY
Como administrador de apis y gateway se utilizo WSO2 Api Manager.

### Cliente
Como cliente, se desarrollo una aplicación web basica en html, css y javascript.

## Requerimientos

⋅⋅* Ubuntu 18.04.4
⋅⋅* Docker
⋅⋅* Python 2.7
⋅⋅* Flask
⋅⋅* Flask_restful
⋅⋅* flask-cors
⋅⋅* WSO2 Api Manager

## Instalación

### Docker
Desde la consola de comandos ejecutar lo siguiente:

1. Actualizar la lista de paquetes
`$ sudo apt update`

2. instalación de paquetes de requisitos previos que le permiten a *apt* usar paquetes mediante HTTPS
`$ sudo apt install apt-transport-https ca-certificates curl software-properties-common`

3. Agregar la clave GPG para el repositorio oficial de Docker a su sistema
`$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -`

4. Agregar el repositorio de Docker a las fuentes de APT
`$ sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"`

5. Actualizar la lista de paquetes con los cambios realizados previamente
`$ sudo apt update`

6. Instalación de Docker
`$ sudo apt install docker-ce`

### APIS

1. Para ejecutar las apis desde un contenedor de docker es necesario crear las imagenes previamente con las dependencias necesarias para la ejecución del api, para esto desde la carpeta **/APIS** ejecutar en una consola de comandos ejecutar el archivo bash BUILD.sh

`$ sudo ./BUILD.sh`

2. A continuación es necesario correr las imagenes creadas en Docker, para esto desde la carpeta **/APIS** ejecutar en una consola de comandos el archivo RUN.sh

`$ sudo ./RUN.sh`

3. Para verificar que todas imagenes se esten ejecutando en docker se ejecuta el siguiente comando 

`$ sudo docker ps`

### WSO2 Api Manager

1. Para la instalación del administrador de apis y gateway se descargar desde la pagina oficial https://wso2.com/api-management/, para este caso la version de Linux Installer deb.

2. Para iniciar la aplicación se ejecuta el siguiente comando
`$ sudo wso2am-x.x.x`

Donde x.x.x es la version instalada.

Para el registro de las apis en el API Manager se puede seguir la guia de inicio rapido de WSO2 https://apim.docs.wso2.com/en/next/getting-started/quick-start-guide/


## Diego Fernando Izquierdo    20201099035
## Mateo Alfonso Puerto        20201099044
