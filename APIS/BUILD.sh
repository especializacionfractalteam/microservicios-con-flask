#!/bin/bash
echo Iniciando la creación de imagenes de docker de las apis

cd APISUM
sudo docker build -t api_flask_sum:latest .
cd ..
cd APIRES
sudo docker build -t api_flask_res:latest .
cd ..
cd APIMULT
sudo docker build -t api_flask_mult:latest .
cd ..
cd APIDIV
sudo docker build -t api_flask_div:latest .
cd ..

echo La creación de las imagenes ha finalizado...
