#!/bin/bash

echo 
d APISUM
sudo docker run -d --name api_flask_sum -p 5000:5000 api_flask_sum
cd ..
cd APIRES
sudo docker run -d --name api_flask_res -p 5001:5000 api_flask_res
cd ..
cd APIMULT
sudo docker run -d --name api_flask_mult -p 5002:5000 api_flask_mult
cd ..
cd APIDIV
sudo docker run -d --name api_flask_div -p 5003:5000 api_flask_div
cd ..

